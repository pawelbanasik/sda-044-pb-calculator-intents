package com.example.rent.sda_044_calculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_KEY = 0;
    public static final String FIRST_KEY = "first key";
    public static final String SECOND_KEY = "second key";
    public static final String RESULT_KEY = "result";
    public static final String ACTION_KEY = "action";

    @BindView(R.id.edit_text_first_number)
    protected EditText editTextFirstNumber;

    @BindView(R.id.edit_text_second_number)
    protected EditText editTextSecondNumber;

    @BindView(R.id.text_view_result)
    protected TextView textViewResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

    }

    public void send(Button button) {
        String firstNumber = editTextFirstNumber.getText().toString();
        String secondNumber = editTextSecondNumber.getText().toString();
        Intent intent = new Intent(this, CalculatingActivity.class);
        intent.putExtra(ACTION_KEY, button.getText().toString());
        intent.putExtra(FIRST_KEY, firstNumber);
        intent.putExtra(SECOND_KEY, secondNumber);
        startActivityForResult(intent, REQUEST_KEY);
    }

    @OnClick(R.id.button_plus)
    protected void plus(Button button) {
        send(button);
    }

    @OnClick(R.id.button_minus)
    protected void minus(Button button) {
        send(button);
    }

    @OnClick(R.id.button_divide)
    protected void divide(Button button) {
        send(button);
    }

    @OnClick(R.id.button_multiply)
    protected void multiply(Button button) {
        send(button);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_KEY && resultCode == RESULT_OK) {
            textViewResult.setText(data.getStringExtra(RESULT_KEY));
        } else {
            // info ze nie ma
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}
