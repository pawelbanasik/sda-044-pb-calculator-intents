package com.example.rent.sda_044_calculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class CalculatingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculating);

        String firstNumber = getIntent().getStringExtra(MainActivity.FIRST_KEY);
        String secondNumber = getIntent().getStringExtra(MainActivity.SECOND_KEY);

        int resultNumber = Integer.parseInt(firstNumber) + Integer.parseInt(secondNumber);
        String result = String.valueOf(resultNumber);

        Intent data = new Intent();
        data.putExtra(MainActivity.RESULT_KEY , result);
        setResult(RESULT_OK, data);
        finish();

    }
}
